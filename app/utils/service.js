const User = require('../model/user');
const jwt = require('jsonwebtoken');
const secret = process.env.secret

const generateToken = (data) => {
    return new Promise((resolve, reject) => {
        jwt.sign({ ...data }, secret, { expiresIn: "20 mins" }, (err, token) => {
            if (err) reject(err);
            else resolve(token)
        })
    })
}

const verifyToken = (token) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, secret, (err, decoded) => {
            if (err) reject({ message: 'Token has expired', status: 401 })
            else {
                resolve(decoded)
            }
        })
    })
}

const decodeToken = (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token']
    const decoded = jwt.decode(token, { complete: true });
    if (decoded) {
        req.auth = { id: decoded.payload._doc._id }
        next()
    } else res.status(403).send({ status: false, message: 'Bad request', status: 403 })
}

const authenticate = (req, res, next) => {
    const token = req.body.token || req.query.token || req.headers['x-access-token']
    if (token) {
        verifyToken(token).then(decode => {
            const userID = decode._doc._id;
            User.findOne({ _id: userID }).then(user => {
                if (!user) throw new Error('User does not exits!');
                return { user };
            })
                .then(({ user }) => {
                    req.auth = {
                        id: user._id
                    }
                    next();
                })
                .catch(err => { res.status(404).send({ success: false, message: err.message }) })
        })
            .catch(err => res.status(err.status).send({ success: false, message: err.message, status: err.status }))
    } else {
        res.status(401).send({ success: false, message: 'No token provided!', status: 401 })
    }
}

module.exports = {
    generateToken, authenticate, decodeToken
}