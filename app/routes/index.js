const authRoutes = require('./userRoutes');

module.exports = (router) => {
    router.use('/user', authRoutes());
    return router;
}