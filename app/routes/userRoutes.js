const router = require('express').Router();
const userController = require('../controllers/userController');
const { authenticate, decodeToken } = require('../utils/service');

module.exports = () => {
    const userCtrl = new userController();
    router.post('/register', userCtrl.register)
    router.post('/login', userCtrl.login) 
    router.get('/profile', authenticate, userCtrl.profile)
    router.get('/getAllJobs', authenticate, userCtrl.getAllJobs)
    router.get('/getAJob/:id', authenticate, userCtrl.getAJob)
    router.post('/applyJob/:id', authenticate, userCtrl.applyJob)
    router.post('/refreshToken', decodeToken, userCtrl.refreshToken)
    router.post('/updateProfile', authenticate, userCtrl.updateProfile)
    router.post('/saveAJob/:id', authenticate, userCtrl.saveAJob)
    router.post('/postAJob', authenticate, userCtrl.postAJob) //Admin only
    router.post('/applicantJobAction/:id/:userID/:action', authenticate, userCtrl.applicantJobAction) //Admin only
    router.get('/getAUser/:userID', authenticate, userCtrl.getAUser) // Admin only
    router.get('/search/:jobName', authenticate, userCtrl.search)
    return router;
}