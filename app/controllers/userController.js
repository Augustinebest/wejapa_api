const service = require('../services/userService');

module.exports = function userController() {
    this.register = (req, res) => {
        service.register(req.body).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.login = (req, res) => {
        service.login(req.body).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.profile = (req, res) => {
        service.profile(req.auth.id).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.getAllJobs = (req, res) => {
        service.getAllJobs().then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.getAJob = (req, res) => {
        const jobID = req.params.id;
        const userID = req.auth.id
        service.getAJob(userID, jobID).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.applyJob = (req, res) => {
        const jobID = req.params.id;
        const authID = req.auth.id
        service.applyJob(authID, jobID).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.refreshToken = (req, res) => {
        service.refreshToken(req.auth.id).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.updateProfile = (req, res) => {
        service.updateProfile(req.auth.id, req.body).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.saveAJob = (req, res) => {
        service.saveAJob(req.auth.id, req.params.id).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.postAJob = (req, res) => {
        service.postAJob(req.body, req.auth.id).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.applicantJobAction = (req, res) => {
        const jobID = req.params.id, userID = req.params.userID, action = req.params.action;
        service.applicantJobAction(req.auth.id, jobID, userID, action).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.getAUser = (req, res) => {
        const userID = req.params.userID;
        service.getAUser(req.auth.id, userID).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }

    this.search = (req, res) => {
        const jobName = req.params.jobName;
        service.search(jobName).then(data => {
            res.status(data.status).send(data);
        }).catch(err => res.status(err.status).send(err));
    }
}