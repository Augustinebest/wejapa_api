const mongoose = require('mongoose');
const schema = mongoose.Schema;

const UserSchema = new schema({
    fullName: {type: String},
    email: {type: String},
    phone: {type: String},
    portfolio: {type: String},
    stackoverflow: {type: String},
    resume: {type: String},
    experience: {type: String},
    password: {type: String},
    role: {type: String, enum: ["admin", "user"]},
    jobs: [{
        jobid: {type: mongoose.SchemaTypes.ObjectId, ref:"jobs"},
        status: {type: String, enum: [null, "progress", "accepted", "rejected"], default: null}
    }],
    savedJobs: [{
        type: mongoose.SchemaTypes.ObjectId, ref:"jobs"
    }]
})

module.exports = mongoose.model('users', UserSchema);