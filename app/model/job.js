const mongoose = require('mongoose');
const schema = mongoose.Schema;

const JobSchema = new schema({
    name: {type: String},
    image: {type: String},
    location: {type: String},
    status: {type: String, enums: ["open", "closed"]},
    workTime: {type: String},
    workType: {type: String},
    salary: {type: String},
    description: {type: String},
    benefits: {type: String},
    experience: {type: String},
    users: [{
        type: mongoose.SchemaTypes.ObjectId, ref:"users"
    }],
})

module.exports = mongoose.model('jobs', JobSchema);