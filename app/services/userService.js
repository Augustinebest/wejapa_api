const User = require('../model/user');
const Job = require('../model/job');
const bcrypt = require('bcryptjs');
const { generateToken } = require('../utils/service');

exports.register = (payload) => {
    return new Promise((resolve, reject) => {
        User.findOne({ email: payload.email }).then(user => {
            if (user) {
                resolve({ success: true, message: 'This email already exists!', status: 403 })
            } else {
                User.findOne({ phone: payload.phone }).then(user => {
                    if (user) {
                        resolve({ success: true, message: 'Phone number already exists!', status: 403 })
                    } else {
                        User.find().then(result => {
                            const user = new User();
                            user.fullName = payload.fullName;
                            user.phone = payload.phone;
                            user.email = payload.email;
                            user.portfolio = payload.portfolio;
                            user.stackoverflow = payload.stackoverflow;
                            user.resume = payload.resume;
                            user.experience = payload.experience;
                            user.password = bcrypt.hashSync(payload.password, 10);
                            user.role = result.length < 1 ? "admin" : "user";
                            user.save().then(newUser => {
                                if (newUser) resolve({ success: true, message: 'User created successfully!', status: 200 })
                            }).catch(err => reject({ success: false, message: err, status: 500 }))
                        }).catch(err => reject({ success: false, message: err, status: 500 }))
                    }
                })
            }
        }).catch(err => reject({ success: false, message: err, status: 500 }))
    })
}

exports.login = (payload) => {
    return new Promise((resolve, reject) => {
        User.findOne({ email: payload.email }).then(user => {
            if (!user) {
                resolve({ success: false, message: 'Invalid credentials', status: 400 })
            } else {
                const checkPassword = bcrypt.compareSync(payload.password, user.password);
                if (!checkPassword) {
                    resolve({ success: false, message: 'Invalid credentials', status: 400 })
                } else {
                    generateToken(user).then(token => {
                        if (!token) throw new Error('Error generating token')
                        return { token }
                    })
                        .then(({ token }) => {
                            resolve({ success: true, message: 'Login successfully!', token, role: user.role, status: 200 })
                        })
                        .catch(err => reject({ success: false, message: err.message, status: 500 }))
                }
            }
        }).catch(err => reject({ success: false, message: err, status: 500 }))
    })
}

exports.profile = (id) => {
    return new Promise((resolve, reject) => {
        User.findOne({ _id: id }, '-password').populate({ path: 'jobs.jobid', select: 'userStats name image location status workType workTime salary description benefits experience' }).then(user => {
            if (!user) resolve({ success: false, message: 'User does not exists', status: 404 })
            resolve({ success: true, message: user, status: 200 })
        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
    })
}

exports.getAllJobs = () => {
    return new Promise((resolve, reject) => {
        Job.find().then(jobs => {
            if (jobs.length < 1) resolve({ success: true, message: 'no jobs currently', status: 404 })
            else {
                resolve({ success: true, message: jobs, status: 200 })
            }
        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
    })
}

exports.getAJob = (userID, jobID) => {
    return new Promise((resolve, reject) => {
        Job.findOne({ _id: jobID }).populate({ path: 'users', select: 'fullName email portfolio stackoverflow phone resume experience' }).then(job => {
            if (!job) resolve({ success: false, message: 'This job does not exists', status: 404 })
            else {
                User.findOne({_id: userID}).then(user => {
                    const checkIfUserApplied = user.jobs.find(job => job.jobid == jobID);
                    if(checkIfUserApplied) {
                        resolve({ success: true, message: {job, userApplied: true }, status: 200 })
                    } else {
                        resolve({ success: true, message: {job, userApplied: false }, status: 200 })
                    }
                }).catch(err => { reject({ success: false, message: err, status: 500 }) })
            }
        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
    })
}

exports.applyJob = (authID, jobID) => {
    return new Promise((resolve, reject) => {
        User.findOne({ _id: authID }, '-password').then(user => {
            Job.findOne({ _id: jobID }).then(job => {
                console.log(job)
                if (user && job) {
                    const checkApplied = user.jobs.find(job => job.jobid == jobID);
                    if (!checkApplied) {
                        User.findOneAndUpdate({ _id: authID }, { $push: { jobs: {jobid: jobID, status: "progress"} } }).exec((err, result) => {
                            if (err) resolve({ success: false, message: 'Error ocurred while applying for this posititon', status: 301 })
                            Job.findOneAndUpdate({ _id: jobID }, { $push: { users: authID } }).exec((err, job) => {
                                if(err) resolve({ success: false, message: 'Error ocurred while applying for this posititon', status: 302 })
                                else {
                                    resolve({ success: true, message: {job, userApplied: true, msg: 'Job applied successfully'}, status: 200 })
                                }
                            })
                        })
                    } else resolve({ success: false, message: 'Applied already', status: 404 })
                } else resolve({ success: false, message: 'Invalid Job or user', status: 404 })
            }).catch(err => { reject({ success: false, message: 'job does not exists', status: 500 }) })
        }).catch(err => { reject({ success: false, message: 'user does not exists', status: 500 }) })
    })
}

exports.refreshToken = (authID) => {
    return new Promise((resolve, reject) => {
        User.findOne({ _id: authID }, '-password').then(user => {
            generateToken(user).then(refreshToken => {
                if (!refreshToken) throw new Error('Error refreshing token');
                resolve({ success: true, message: refreshToken, status: 200 })
            }).catch(err => { reject({ success: false, message: err.message, status: 500 }) })
        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
    })
}

exports.updateProfile = (authID, payload) => {
    return new Promise((resolve, reject) => {
        const updates = {
            phone: payload.phone,
            portfolio: payload.portfolio,
        }
        User.findOneAndUpdate({ _id: authID }, updates).exec((err, update) => {
            if (err) reject({ success: false, message: err, status: 500 });
            if (update) {
                resolve({ success: true, message: 'Profile updated successfully', status: 200 })
            } else resolve({ success: false, message: 'Error occured in updating profile', status: 404 })
        })
    })
}

exports.saveAJob = (authID, jobID) => {
    return new Promise((resolve, reject) => {
        User.findOne({_id: authID}).then(user => {
            if(!user) resolve({success: false, message: 'User does not exists', status: 400})
            Job.findOne({_id: jobID}).then(job => {
                if(!job) resolve({success: false, message: 'This job does not exists', status: 404})
                if(job && user) {
                    const checkIfUserApplied = user.jobs.find(job => job == jobID)
                    console.log(checkIfUserApplied);
                    if(!checkIfUserApplied) {
                        Job.findOneAndUpdate({_id: jobID}).then(saved => {
                            resolve({success: true, message: 'job saved successfully', status: 200 })
                        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
                    }
                }
            }).catch(err => { reject({ success: false, message: err, status: 500 }) })
        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
    })
}

// Admin Only
exports.postAJob = (payload, id) => {
    return new Promise((resolve, reject) => {
        User.findOne({ _id: id }, '-password').then(admin => {
            if (admin.role === 'admin') {
                const job = new Job();
                job.name = payload.name;
                job.image = payload.image;
                job.location = payload.location;
                job.status = payload.status;
                job.workType = payload.workType;
                job.workTime = payload.workTime;
                job.salary = payload.salary;
                job.description = payload.description;
                job.benefits = payload.benefits;
                job.experience = payload.experience;
                job.save().then(newJob => {
                    if (newJob) resolve({ success: true, message: 'New job posted successfully', status: 200 })
                }).catch(err => { reject({ success: false, message: err, status: 500 }) })
            } else {
                reject({ success: false, message: 'Bad Request', status: 403 })
            }
        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
    })
}

// Admin only
exports.applicantJobAction = (adminID, jobID, userID, action) => {
    return new Promise((resolve, reject) => {
        User.findOne({ _id: adminID }).then(admin => {
            if (admin.role === 'admin') {
                Job.findOne({ _id: jobID }).populate({ path: 'users', select: 'fullName email portfolio stackoverflow phone resume experience jobs' }).then(job => {
                    const checkIfUserApplied = job.users.find(user => user._id == userID);
                    if (checkIfUserApplied) {
                        User.findOne({_id: userID}).then(user => {
                            let findTheJob = user.jobs.find(job => job.jobid == jobID);
                            findTheJob.status = `${action}`;
                            user.save().then(save => {
                                resolve({success: true, message: `${action} action done successfully`, status: 200 })
                            }).catch(err => { reject({ success: false, message: 'err', status: 500 }) })
                        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
                    } else { resolve({ success: false, message: "User didn't apply for this position", status: 404 }) }
                }).catch(err => { reject({ success: false, message: err, status: 500 }) })
            } else {
                reject({ success: false, message: 'Bad Request', status: 403 })
            }
        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
    })
}

// Admin get a user
exports.getAUser = (adminID, userID) => {
    return new Promise((resolve, reject) => {
        User.findOne({ _id: adminID }).then(admin => {
            if (admin.role === 'admin') {
                User.findOne({ _id: userID }, '-password').populate({ path: 'jobs.jobid', select: 'userStats name image location status workType workTime salary description benefits experience' }).then(user => {
                    if (!user) resolve({ success: false, message: 'User does not exists', status: 404 })
                    resolve({ success: true, message: user, status: 200 })
                }).catch(err => { reject({ success: false, message: err, status: 500 }) })
            } else {
                reject({ success: false, message: 'Bad Request', status: 403 })
            }
        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
    })
}

exports.search = (jobName) => {
    return new Promise((resolve, reject) => {
        console.log(jobName);
        Job.find({$or: [{'name': {$regex: jobName, $options: 'i'}}]}, 'name image location status salary').then(job => {
            if(job.length > 0) {
                resolve({success: true, message: job, status: 200})
            } else {
                resolve({success: true, message: `${jobName} not found`, status: 404})
            }
        }).catch(err => { reject({ success: false, message: err, status: 500 }) })
    })
}