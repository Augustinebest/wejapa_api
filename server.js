require('dotenv').config();
const express = require('express');
const bodyparser = require('body-parser');
const morgan = require('morgan');
const mongoose = require('mongoose');
const cors = require('cors');
const router = express.Router();
const rootRouter = require('./app/routes/index')(router);

const app = express();
const PORT = process.env.PORT || 9001;
const db = process.env.NODE_ENV === "production" ? process.env.PROD_DB : process.env.PROD_DB;

// connection to database
mongoose.connect(db, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}, (err) => {
    if(err) console.log('Database connection failed!');
    else {console.log('Successfully connected to database!')}
})

// Middlewares
app.use(morgan('dev'));
app.use(bodyparser.urlencoded({ extended: true }));
app.use(bodyparser.json());
app.use(express.json()); //for parsing application/json
app.use(express.urlencoded({ extended: false })); //for parsing application/x-www-form-urlencoded
app.use(cors());

// api usage
app.use('/api', rootRouter)

app.all('*', (req, res) => res.status(200).send({ message: 'server is live' }));

app.listen(PORT, () => {
    console.log(`app running on POORT: ${PORT}`)
})